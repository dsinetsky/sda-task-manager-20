package ru.t1.dsinetsky.tm.constant;

public final class TerminalConst {

    public static final String CMD_HELP = "help";

    public static final String CMD_PROJECT_HELP = "prj-help";

    public static final String CMD_TASK_HELP = "task-help";

    public static final String CMD_SYSTEM_HELP = "system-help";

    public static final String CMD_USER_HELP = "user-help";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_VERSION = "version";

    public static final String CMD_INFO = "info";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_ARG = "arg";

    public static final String CMD_CMD = "cmd";

    public static final String CMD_PROJECT_CLEAR = "prj-clear";

    public static final String CMD_PROJECT_CREATE = "prj-create";

    public static final String CMD_PROJECT_LIST = "prj-show";

    public static final String CMD_TASK_CLEAR = "task-clear";

    public static final String CMD_TASK_CREATE = "task-create";

    public static final String CMD_TASK_LIST = "task-show";

    public static final String CMD_FIND_PROJECT_BY_ID = "prj-find-id";

    public static final String CMD_FIND_PROJECT_BY_INDEX = "prj-find-index";

    public static final String CMD_UPD_PROJECT_BY_ID = "prj-upd-id";

    public static final String CMD_UPD_PROJECT_BY_INDEX = "prj-upd-index";

    public static final String CMD_REMOVE_PROJECT_BY_ID = "prj-remove-id";

    public static final String CMD_REMOVE_PROJECT_BY_INDEX = "prj-remove-index";

    public static final String CMD_CREATE_TEST_PROJECTS = "prj-test";

    public static final String CMD_PROJECT_START_BY_ID = "prj-start-id";

    public static final String CMD_PROJECT_START_BY_INDEX = "prj-start-index";

    public static final String CMD_PROJECT_COMPLETE_ID = "prj-complete-id";

    public static final String CMD_PROJECT_COMPLETE_INDEX = "prj-complete-index";

    public static final String CMD_PROJECT_CHANGE_STATUS_BY_ID = "prj-status-id";

    public static final String CMD_PROJECT_CHANGE_STATUS_BY_INDEX = "prj-status-index";

    public static final String CMD_FIND_TASK_BY_ID = "task-find-id";

    public static final String CMD_FIND_TASK_BY_INDEX = "task-find-index";

    public static final String CMD_UPD_TASK_BY_ID = "task-upd-id";

    public static final String CMD_UPD_TASK_BY_INDEX = "task-upd-index";

    public static final String CMD_REMOVE_TASK_BY_ID = "task-remove-id";

    public static final String CMD_REMOVE_TASK_BY_INDEX = "task-remove-index";

    public static final String CMD_CREATE_TEST_TASKS = "task-test";

    public static final String CMD_TASK_START_BY_ID = "task-start-id";

    public static final String CMD_TASK_START_BY_INDEX = "task-start-index";

    public static final String CMD_TASK_COMPLETE_ID = "task-complete-id";

    public static final String CMD_TASK_COMPLETE_INDEX = "task-complete-index";

    public static final String CMD_TASK_CHANGE_STATUS_BY_ID = "task-status-id";

    public static final String CMD_TASK_CHANGE_STATUS_BY_INDEX = "task-status-index";

    public static final String CMD_BIND_TASK_TO_PROJECT = "bind-task";

    public static final String CMD_UNBIND_TASK_TO_PROJECT = "unbind-task";

    public static final String CMD_LIST_TASKS_OF_PROJECT = "list-project-tasks";

    public static final String CMD_LOGIN_USER = "user-login";

    public static final String CMD_LOGOUT_USER = "user-logout";

    public static final String CMD_REGISTRY_USER = "user-registry";

    public static final String CMD_UPDATE_CURRENT_USER = "user-update";

    public static final String CMD_CHANGE_PASSWORD = "user-change-pass";

    public static final String CMD_UPDATE_CURRENT_USER_EMAIL = "user-email-update";

    public static final String CMD_CREATE_USER = "user-create";

    public static final String CMD_UPDATE_USER_EMAIL_BY_ID = "user-update-email-id";

    public static final String CMD_UPDATE_USER_EMAIL_BY_LOGIN = "user-update-email-login";

    public static final String CMD_UPDATE_USER_BY_ID = "user-update-id";

    public static final String CMD_UPDATE_USER_BY_LOGIN = "user-update-login";

    public static final String CMD_SHOW_USER = "user-show";

}
