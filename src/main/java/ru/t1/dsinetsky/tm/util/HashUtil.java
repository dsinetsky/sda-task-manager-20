package ru.t1.dsinetsky.tm.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    String SECRET = "18854";

    int ITERATION = 7777;

    static String salt(final String value) {
        if (value == null) return null;
        String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5Hash(SECRET + result + SECRET);
        }
        return result;
    }

    static String md5Hash(final String value) {
        if (value == null) return null;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            final byte[] array = messageDigest.digest(value.getBytes());
            final StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                stringBuffer.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
