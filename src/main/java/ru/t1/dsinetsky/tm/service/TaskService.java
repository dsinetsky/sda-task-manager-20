package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.TaskNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.*;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.model.User;

import java.util.List;
import java.util.Random;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String userId, final String name) throws GeneralException {
        isUserIdEmpty(userId);
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId, final String name, final String description) throws GeneralException {
        isUserIdEmpty(userId);
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescIsEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) throws GeneralException {
        isUserIdEmpty(userId);
        if (id == null || id.isEmpty()) throw new TaskIdIsEmptyException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        final Task task = findById(userId, id);
        task.setName(name);
        task.setDesc(description);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final int index, final String name, final String description) throws GeneralException {
        isUserIdEmpty(userId);
        if (index < 0) throw new NegativeIndexException();
        if (index >= getSize(userId)) throw new IndexOutOfSizeException(getSize(userId));
        final Task task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        task.setName(name);
        task.setDesc(description);
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) throws GeneralException {
        isUserIdEmpty(userId);
        if (id == null || id.isEmpty()) throw new TaskIdIsEmptyException();
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, final int index, final Status status) throws GeneralException {
        isUserIdEmpty(userId);
        if (index < 0) throw new NegativeIndexException();
        if (index >= getSize(userId)) throw new IndexOutOfSizeException(getSize(userId));
        final Task task = findByIndex(userId, index);
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> returnTasksOfProject(final String userId, final String projectId) throws GeneralException {
        isUserIdEmpty(userId);
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdIsEmptyException();
        return repository.findTasksByProjectId(userId, projectId);
    }

    @Override
    public void createTest(final User user) throws GeneralException {
        final Random randomizer = new Random();
        final int count = 10;
        final String name = user.getLogin() +"-task";
        final String description = "Task for " + user.getLogin() + " number ";
        for (int i = 1; i < count; i++) {
            final Task task = create(user.getId(),name + randomizer.nextInt(11), description + i);
            final Status status;
            switch (randomizer.nextInt(3)) {
                case 1:
                    status = Status.IN_PROGRESS;
                    break;
                case 2:
                    status = Status.COMPLETED;
                    break;
                default:
                    status = Status.NOT_STARTED;
                    break;
            }
            task.setStatus(status);
        }
    }

}
