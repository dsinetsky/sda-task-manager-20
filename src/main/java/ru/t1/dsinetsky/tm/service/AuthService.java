package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.service.IAuthService;
import ru.t1.dsinetsky.tm.api.service.IUserService;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.user.*;
import ru.t1.dsinetsky.tm.model.User;
import ru.t1.dsinetsky.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(String login, String password) throws GeneralException {
        return userService.create(login, password);
    }

    @Override
    public void login(String login, String password) throws GeneralUserException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (password == null || password.isEmpty()) throw new UserPasswordIsEmptyException();
        if (!userService.isUserExistByLogin(login)) throw new IncorrectLoginPasswordException();
        final User user = userService.findUserByLogin(login);
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() throws GeneralUserException {
        if (!isAuth()) throw new UserNotLoggedException();
        return userId;
    }

    @Override
    public User getUser() throws GeneralException {
        if (!isAuth()) throw new UserNotLoggedException();
        final User user = userService.findById(userId);
        if (user == null) throw new UserNotLoggedException();
        return user;
    }

    @Override
    public void checkRoles(Role[] roles) throws GeneralException {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        final boolean hasAccess = Arrays.asList(roles).contains(role);
        if (!hasAccess) throw new AccessDeniedException();
    }

}
