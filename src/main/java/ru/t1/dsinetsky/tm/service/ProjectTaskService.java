package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.service.IProjectTaskService;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dsinetsky.tm.exception.entity.TaskNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.IndexOutOfSizeException;
import ru.t1.dsinetsky.tm.exception.field.NegativeIndexException;
import ru.t1.dsinetsky.tm.exception.field.ProjectIdIsEmptyException;
import ru.t1.dsinetsky.tm.exception.field.TaskIdIsEmptyException;
import ru.t1.dsinetsky.tm.exception.user.UserNotLoggedException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindProjectById(final String userId, final String projectId, final String taskId) throws GeneralException {
        if(userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdIsEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdIsEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void unbindProjectById(final String userId, final String projectId, final String taskId) throws GeneralException {
        if(userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdIsEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdIsEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

    @Override
    public Project removeProjectById(final String userId, final String projectId) throws GeneralException {
        if(userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdIsEmptyException();
        final Project project = projectRepository.findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findTasksByProjectId(userId, projectId);
        for (Task task : tasks) taskRepository.removeById(userId, task.getId());
        projectRepository.removeById(userId, projectId);
        return project;
    }

    @Override
    public Project removeProjectByIndex(final String userId, final int index) throws GeneralException {
        if(userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (index < 0) throw new NegativeIndexException();
        if (index >= projectRepository.getSize(userId)) throw new IndexOutOfSizeException(projectRepository.getSize(userId));
        final Project project = projectRepository.findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findTasksByProjectId(userId, project.getId());
        for (Task task : tasks) taskRepository.removeById(userId, task.getId());
        projectRepository.removeByIndex(userId, index);
        return project;
    }

}
