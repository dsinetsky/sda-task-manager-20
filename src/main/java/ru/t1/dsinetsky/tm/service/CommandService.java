package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.ICommandRepository;
import ru.t1.dsinetsky.tm.api.service.ICommandService;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.command.project.AbstractProjectCommand;
import ru.t1.dsinetsky.tm.command.system.AbstractSystemCommand;
import ru.t1.dsinetsky.tm.command.task.AbstractTaskCommand;
import ru.t1.dsinetsky.tm.command.user.AbstractUserCommand;
import ru.t1.dsinetsky.tm.exception.system.InvalidArgumentException;
import ru.t1.dsinetsky.tm.exception.system.InvalidCommandException;

import java.util.ArrayList;
import java.util.Collection;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public Collection<AbstractCommand> getProjectCommands() {
        final Collection<AbstractCommand> resultCollection = new ArrayList<>();
        for (final AbstractCommand command : getTerminalCommands()) {
            if (command.getName() != null && !command.getName().isEmpty() && command instanceof AbstractProjectCommand)
                resultCollection.add(command);
        }
        return resultCollection;
    }

    @Override
    public Collection<AbstractCommand> getTaskCommands() {
        final Collection<AbstractCommand> resultCollection = new ArrayList<>();
        for (final AbstractCommand command : getTerminalCommands()) {
            if (command.getName() != null && !command.getName().isEmpty() && command instanceof AbstractTaskCommand)
                resultCollection.add(command);
        }
        return resultCollection;
    }

    @Override
    public Collection<AbstractCommand> getUserCommands() {
        final Collection<AbstractCommand> resultCollection = new ArrayList<>();
        for (final AbstractCommand command : getTerminalCommands()) {
            if (command.getName() != null && !command.getName().isEmpty() && command instanceof AbstractUserCommand)
                resultCollection.add(command);
        }
        return resultCollection;
    }

    @Override
    public Collection<AbstractCommand> getSystemCommands() {
        final Collection<AbstractCommand> resultCollection = new ArrayList<>();
        for (final AbstractCommand command : getTerminalCommands()) {
            if (command.getName() != null && !command.getName().isEmpty() && command instanceof AbstractSystemCommand)
                resultCollection.add(command);
        }
        return resultCollection;
    }

    @Override
    public void add(AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByName(String commandName) throws InvalidCommandException {
        if (commandName == null || commandName.isEmpty()) throw new InvalidCommandException();
        final AbstractCommand command = commandRepository.getCommandByName(commandName);
        if (command == null) throw new InvalidCommandException(commandName);
        return command;
    }

    @Override
    public AbstractCommand getCommandByArgument(String argument) throws InvalidArgumentException {
        if (argument == null || argument.isEmpty()) throw new InvalidArgumentException();
        final AbstractCommand command = commandRepository.getCommandByArgument(argument);
        if (command == null) throw new InvalidArgumentException(argument);
        return command;
    }

}
