package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.dsinetsky.tm.api.service.IAbstractUserOwnedService;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.EntityNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.EntityIdIsEmptyException;
import ru.t1.dsinetsky.tm.exception.field.IndexOutOfSizeException;
import ru.t1.dsinetsky.tm.exception.field.NegativeIndexException;
import ru.t1.dsinetsky.tm.exception.user.UserNotLoggedException;
import ru.t1.dsinetsky.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IAbstractUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IAbstractUserOwnedService<M> {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    protected void isUserIdEmpty(final String userId) throws UserNotLoggedException {
        if(userId == null || userId.isEmpty()) throw new UserNotLoggedException();
    }

    @Override
    public List<M> returnAll(final String userId) throws GeneralException {
        isUserIdEmpty(userId);
        return repository.returnAll(userId);
    }

    @Override
    public List<M> returnAll(final String userId, final Comparator comparator) throws GeneralException {
        if (comparator == null) return returnAll(userId);
        return repository.returnAll(userId, comparator);
    }

    @Override
    public List<M> returnAll(final String userId, final Sort sort) throws GeneralException {
        isUserIdEmpty(userId);
        if (sort == null) return returnAll(userId);
        return repository.returnAll(userId, sort.getComparator());
    }

    @Override
    public M add(final String userId, final M model) throws GeneralException {
        isUserIdEmpty(userId);
        if (model == null) throw new EntityNotFoundException(getModelType());
        return repository.add(userId, model);
    }

    @Override
    public void clear(final String userId) throws GeneralException {
        isUserIdEmpty(userId);
        repository.clear(userId);
    }

    @Override
    public M findById(final String userId, final String id) throws GeneralException {
        isUserIdEmpty(userId);
        if (id == null || id.isEmpty()) throw new EntityIdIsEmptyException(getModelType());
        final M model = repository.findById(userId, id);
        if (model == null) throw new EntityNotFoundException(getModelType());
        return model;
    }

    @Override
    public M findByIndex(final String userId, final int index) throws GeneralException {
        isUserIdEmpty(userId);
        if (index < 0) throw new NegativeIndexException();
        if (index >= repository.getSize(userId)) throw new IndexOutOfSizeException(repository.getSize(userId));
        return repository.findByIndex(userId, index);
    }

    @Override
    public M removeById(final String userId, final String id) throws GeneralException {
        isUserIdEmpty(userId);
        if (id == null || id.isEmpty()) throw new EntityIdIsEmptyException(getModelType());
        final M model = repository.removeById(userId, id);
        if (model == null) throw new EntityNotFoundException(getModelType());
        return model;
    }

    @Override
    public M removeByIndex(final String userId, final int index) throws GeneralException {
        isUserIdEmpty(userId);
        if (index < 0) throw new NegativeIndexException();
        if (index >= repository.getSize(userId)) throw new IndexOutOfSizeException(repository.getSize(userId));
        return repository.removeByIndex(userId, index);
    }

    @Override
    public boolean existsById(final String userId, final String id) throws GeneralException {
        isUserIdEmpty(userId);
        if (id == null || id.isEmpty()) throw new EntityIdIsEmptyException(getModelType());
        return repository.existsById(userId, id);
    }

    @Override
    public int getSize(final String userId) throws GeneralException {
        return repository.getSize(userId);
    }

}
