package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.field.*;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.model.User;

import java.util.Random;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId, final String name) throws GeneralException {
        isUserIdEmpty(userId);
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Project create(final String userId, final String name, final String description) throws GeneralException {
        isUserIdEmpty(userId);
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescIsEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) throws GeneralException {
        isUserIdEmpty(userId);
        if (id == null || id.isEmpty()) throw new ProjectIdIsEmptyException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        final Project project = findById(userId, id);
        project.setName(name);
        project.setDesc(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final int index, final String name, final String description) throws GeneralException {
        isUserIdEmpty(userId);
        if (index < 0) throw new NegativeIndexException();
        if (index >= getSize(userId)) throw new IndexOutOfSizeException(getSize(userId));
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        final Project project = findByIndex(userId,index);
        project.setName(name);
        project.setDesc(description);
        return project;
    }

    @Override
    public Project changeStatusById(final String userId, final String id, final Status status) throws GeneralException {
        isUserIdEmpty(userId);
        if (id == null || id.isEmpty()) throw new ProjectIdIsEmptyException();
        final Project project = findById(userId, id);
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final String userId, final int index, final Status status) throws GeneralException {
        isUserIdEmpty(userId);
        if (index < 0) throw new NegativeIndexException();
        if (index >= getSize(userId)) throw new IndexOutOfSizeException(getSize(userId));
        final Project project = findByIndex(userId, index);
        project.setStatus(status);
        return project;
    }

    @Override
    public void createTest(final User user) throws GeneralException {
        final Random randomizer = new Random();
        final int count = 10;
        final String name = user.getLogin() + "-project-";
        final String description = "Project for " + user.getLogin() + " number ";
        for (int i = 1; i < count; i++) {
            final Project project = create(user.getId(),name + randomizer.nextInt(11), description + i);
            final Status status;
            switch (randomizer.nextInt(3)) {
                case 1:
                    status = Status.IN_PROGRESS;
                    break;
                case 2:
                    status = Status.COMPLETED;
                    break;
                default:
                    status = Status.NOT_STARTED;
                    break;
            }
            project.setStatus(status);
        }
    }

}
