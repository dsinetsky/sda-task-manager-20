package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IAbstractUserOwnedRepository<M> {

    protected final List<M> models = new ArrayList<>();

    protected boolean isUserIdEmpty(final String userId) {
        return userId == null || userId.isEmpty();
    }

    @Override
    public List<M> returnAll(final String userId) {
        if(isUserIdEmpty(userId)) return Collections.emptyList();
        final List<M> userOwnedModels = new ArrayList<>();
        for (M model : models) {
            if(model.getUserId().equals(userId)) userOwnedModels.add(model);
        }
        return userOwnedModels;
    }

    @Override
    public List<M> returnAll(final String userId, final Comparator comparator) {
        if(isUserIdEmpty(userId)) return Collections.emptyList();
        final List<M> sortedList = new ArrayList<>(returnAll(userId));
        sortedList.sort(comparator);
        return sortedList;
    }

    @Override
    public void clear(final String userId) {
        if(isUserIdEmpty(userId)) return;
        models.removeAll(returnAll(userId));
    }

    @Override
    public M findById(final String userId, final String id) throws GeneralException {
        if(isUserIdEmpty(userId)) return null;
        final List<M> userOwnedModels = returnAll(userId);
        for (M model : userOwnedModels) {
            if (model.getId().equals(id)) return model;
        }
        return null;
    }

    @Override
    public M findByIndex(final String userId, final int index) throws GeneralException {
        return returnAll(userId).get(index);
    }

    @Override
    public M removeById(final String userId, final String id) throws GeneralException {
        final M model = findById(userId,id);
        models.remove(model);
        return model;
    }

    @Override
    public M removeByIndex(final String userId, final int index) throws GeneralException {
        final M model = findByIndex(userId,index);
        models.remove(model);
        return model;
    }

    @Override
    public boolean existsById(final String userId, final String id) throws GeneralException {
        return findById(userId,id) != null;
    }

    @Override
    public int getSize(final String userId) {
        return returnAll(userId).size();
    }

    @Override
    public M add(final String userId, final M model){
        if(isUserIdEmpty(userId)) return null;
        model.setUserId(userId);
        models.add(model);
        return model;
    }

}
