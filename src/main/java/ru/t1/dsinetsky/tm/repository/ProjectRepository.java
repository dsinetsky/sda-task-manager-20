package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.model.AbstractUserOwnedModel;
import ru.t1.dsinetsky.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String userId, final String name) {
        if (isUserIdEmpty(userId)) return null;
        if (name == null || name.isEmpty()) return null;
        return add(userId, new Project(name));
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        if (isUserIdEmpty(userId)) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return add(userId, new Project(name, description));
    }

}
