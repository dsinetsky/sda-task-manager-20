package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.repository.IUserRepository;
import ru.t1.dsinetsky.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User add(final User user) {
        if (user == null) return null;
        if (existsById(user.getId())) return null;
        final String login = user.getLogin();
        if (login == null || login.isEmpty()) return null;
        if (isUserLoginExist(login)) return null;
        final String password = user.getPasswordHash();
        if (password == null || password.isEmpty()) return null;
        models.add(user);
        return user;
    }

    @Override
    public User findUserByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        for (final User search : models) {
            if (search.getLogin().equals(login)) return search;
        }
        return null;
    }

    @Override
    public boolean isUserLoginExist(final String login) {
        return findUserByLogin(login) != null;
    }

    @Override
    public void removeByLogin(final String login) {
        if (login == null || login.isEmpty()) return;
        final User user = findUserByLogin(login);
        if (user == null) return;
        models.remove(user);
    }

}
