package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.model.AbstractUserOwnedModel;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findTasksByProjectId(final String userId, final String projectId) {
        if (isUserIdEmpty(userId)) return Collections.emptyList();
        final List<Task> result = new ArrayList<>();
        for (final Task task : returnAll(userId)) {
            if (projectId.equals(task.getProjectId())) result.add(task);
        }
        return result;
    }

    @Override
    public Task create(final String userId, final String name) {
        if (isUserIdEmpty(userId)) return null;
        if (name == null || name.isEmpty()) return null;
        return add(userId, new Task(name));
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        if (isUserIdEmpty(userId)) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return add(userId, new Task(name, description));
    }

}
