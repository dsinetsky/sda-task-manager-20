package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.repository.IAbstractRepository;
import ru.t1.dsinetsky.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IAbstractRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public List<M> returnAll() {
        return models;
    }

    @Override
    public List<M> returnAll(final Comparator comparator) {
        final List<M> sortedList = new ArrayList<>(models);
        sortedList.sort(comparator);
        return sortedList;
    }

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public M findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        for (final M model : models) {
            if (id.equals(model.getId())) return model;
        }
        return null;
    }

    @Override
    public M findByIndex(final int index) {
        if (index >= models.size()) return null;
        return models.get(index);
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        final M model = findById(id);
        if (model == null) return null;
        else models.remove(model);
        return model;
    }

    @Override
    public M removeByIndex(final int index) {
        final M model = findByIndex(index);
        if (model == null) return null;
        else models.remove(model);
        return model;
    }

    @Override
    public boolean existsById(final String id) {
        return findById(id) != null;
    }

    @Override
    public int getSize() {
        return models.size();
    }

}
