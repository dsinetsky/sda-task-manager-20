package ru.t1.dsinetsky.tm.command.user;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String NAME = TerminalConst.CMD_CHANGE_PASSWORD;

    public static final String DESCRIPTION = "Change password of current user";

    @Override
    public void execute() throws GeneralException {
        final String id = getUserId();
        System.out.println("Enter new password:");
        final String password = TerminalUtil.nextLine();
        getUserService().changePassword(id, password);
        System.out.println("User successfully updated!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
