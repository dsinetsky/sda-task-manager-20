package ru.t1.dsinetsky.tm.command.task;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    public static final String NAME = TerminalConst.CMD_TASK_CHANGE_STATUS_BY_ID;

    public static final String DESCRIPTION = "Changes status of task (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of task:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatusList());
        final String newStatus = TerminalUtil.nextLine();
        final Status status = Status.toStatus(newStatus);
        final String userId = getUserId();
        final Task task = getTaskService().changeStatusById(userId, id, status);
        showTask(task);
        System.out.println("Status successfully changed");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
