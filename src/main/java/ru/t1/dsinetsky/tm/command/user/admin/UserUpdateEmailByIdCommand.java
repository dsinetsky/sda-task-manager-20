package ru.t1.dsinetsky.tm.command.user.admin;

import ru.t1.dsinetsky.tm.command.user.AbstractUserCommand;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserUpdateEmailByIdCommand extends AbstractAdminCommand {

    public static final String NAME = TerminalConst.CMD_UPDATE_USER_EMAIL_BY_ID;

    public static final String DESCRIPTION = "Updates email of user (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of user:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter new email:");
        final String email = TerminalUtil.nextLine();
        getUserService().updateEmailById(id, email);
        System.out.println("User email successfully updated!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
