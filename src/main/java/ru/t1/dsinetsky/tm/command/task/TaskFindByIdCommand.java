package ru.t1.dsinetsky.tm.command.task;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskFindByIdCommand extends AbstractTaskCommand {

    public static final String NAME = TerminalConst.CMD_FIND_TASK_BY_ID;

    public static final String DESCRIPTION = "Show task (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of task:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Task task = getTaskService().findById(userId, id);
        showTask(task);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
