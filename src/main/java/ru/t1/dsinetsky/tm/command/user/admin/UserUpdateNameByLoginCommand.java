package ru.t1.dsinetsky.tm.command.user.admin;

import ru.t1.dsinetsky.tm.command.user.AbstractUserCommand;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserUpdateNameByLoginCommand extends AbstractAdminCommand {

    public static final String NAME = TerminalConst.CMD_UPDATE_USER_BY_LOGIN;

    public static final String DESCRIPTION = "Updates first, middle and last name of user (if any) found by login";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter login of user:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter new first name:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter new last name:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter new middle name:");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUserByLogin(login, firstName, lastName, middleName);
        System.out.println("User name successfully updated!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
