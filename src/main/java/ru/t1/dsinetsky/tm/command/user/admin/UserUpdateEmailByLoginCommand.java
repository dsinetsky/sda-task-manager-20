package ru.t1.dsinetsky.tm.command.user.admin;

import ru.t1.dsinetsky.tm.command.user.AbstractUserCommand;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserUpdateEmailByLoginCommand extends AbstractAdminCommand {

    public static final String NAME = TerminalConst.CMD_UPDATE_USER_EMAIL_BY_LOGIN;

    public static final String DESCRIPTION = "Updates email of user (if any) found by login";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter login of user:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter new email:");
        final String email = TerminalUtil.nextLine();
        getUserService().updateEmailByLogin(login, email);
        System.out.println("User email successfully updated!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
