package ru.t1.dsinetsky.tm.command.project;

import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.api.service.IProjectTaskService;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    public void showProject(Project project) {
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Desc: " + project.getDesc());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
