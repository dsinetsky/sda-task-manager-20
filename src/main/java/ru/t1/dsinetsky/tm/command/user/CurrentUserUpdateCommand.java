package ru.t1.dsinetsky.tm.command.user;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class CurrentUserUpdateCommand extends AbstractUserCommand {

    public static final String NAME = TerminalConst.CMD_UPDATE_CURRENT_USER;

    public static final String DESCRIPTION = "Update name of current user";

    @Override
    public void execute() throws GeneralException {
        final String id = getUserId();
        System.out.println("Enter new first name:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter new last name:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter new middle name:");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUserById(id, firstName, lastName, middleName);
        System.out.println("User successfully updated!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
