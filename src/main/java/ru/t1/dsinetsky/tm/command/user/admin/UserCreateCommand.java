package ru.t1.dsinetsky.tm.command.user.admin;

import ru.t1.dsinetsky.tm.builder.repository.UserBuilder;
import ru.t1.dsinetsky.tm.command.user.AbstractUserCommand;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.User;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserCreateCommand extends AbstractAdminCommand {

    public static final String NAME = TerminalConst.CMD_CREATE_USER;

    public static final String DESCRIPTION = "Creates new user";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter login of user:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        System.out.println("Enter user role");
        final String roleValue = TerminalUtil.nextLine();
        final Role role = Role.toRole(roleValue);
        System.out.println("Enter email:");
        final String email = TerminalUtil.nextLine();
        System.out.println("Enter first name:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        final String middleName = TerminalUtil.nextLine();
        final User user = UserBuilder.create()
                .login(login)
                .password(password)
                .role(role)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .middleName(middleName)
                .toUser();
        getUserService().add(user);
        System.out.println("User successfully created!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
