package ru.t1.dsinetsky.tm.command.task;

import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class BindTaskToProjectCommand extends AbstractTaskCommand {

    public static final String NAME = TerminalConst.CMD_BIND_TASK_TO_PROJECT;

    public static final String DESCRIPTION = "Binds task to project(if any)";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter Id of project:");
        final String projectEnter = TerminalUtil.nextLine();
        System.out.println("Enter Id of task:");
        final String taskEnter = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectTaskService().bindProjectById(userId, projectEnter, taskEnter);
        System.out.println("Task successfully bound to project!");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
