package ru.t1.dsinetsky.tm.command.system;

import ru.t1.dsinetsky.tm.constant.TerminalConst;

public final class ExitCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = null;

    public static final String NAME = TerminalConst.CMD_EXIT;

    public static final String DESCRIPTION = "Exit application";

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
