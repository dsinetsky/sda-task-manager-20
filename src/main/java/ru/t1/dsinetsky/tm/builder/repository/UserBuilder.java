package ru.t1.dsinetsky.tm.builder.repository;

import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.model.User;

public final class UserBuilder {

    User user = new User();

    private UserBuilder() {
    }

    public static UserBuilder create() {
        return new UserBuilder();
    }

    public UserBuilder login(final String login) {
        user.setLogin(login);
        return this;
    }

    public UserBuilder email(final String email) {
        user.setEmail(email);
        return this;
    }

    public UserBuilder firstName(final String firstName) {
        user.setFirstName(firstName);
        return this;
    }

    public UserBuilder lastName(final String lastName) {
        user.setLastName(lastName);
        return this;
    }

    public UserBuilder middleName(final String middleName) {
        user.setMiddleName(middleName);
        return this;
    }

    public UserBuilder password(final String password) {
        user.setPasswordHash(password);
        return this;
    }

    public UserBuilder role(final Role role) {
        user.setRole(role);
        return this;
    }

    public User toUser() {
        return user;
    }


}
