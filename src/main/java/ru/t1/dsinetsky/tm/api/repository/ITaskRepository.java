package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IAbstractUserOwnedRepository<Task> {

    Task create(String userId, String name);

    Task create(String userId, String name, String desc);

    List<Task> findTasksByProjectId(String userId, String projectId);

}
