package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.api.repository.IAbstractRepository;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.AbstractModel;

import java.util.List;

public interface IAbstractService<M extends AbstractModel> extends IAbstractRepository<M> {

    List<M> returnAll(Sort sort);

}
